/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';2
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image
} from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Detail from './screens/detail'
import Menu from './screens/menu'
import Plash from './screens/plash'

const Stack = createStackNavigator();

const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Plash" component={Plash} options={{headerShown: false}}/>
        <Stack.Screen name="Menu" component={Menu} options={{headerShown: false }}/>
        <Stack.Screen name="Details" component={Detail}
          options={{
            title: 'SCANED PRODUCT',
            headerTitleStyle: { 
              marginLeft: 50, 
            },
            headerTintColor: '#f87b48',
            headerStyle: {
              elevation: 0,
              backgroundColor: '#FAF0E6'
            },
          }} 
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  
});

export default App;
