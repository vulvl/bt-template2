import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';

const Items = () => {
  let itemArr = []
  listItem.map(item => {
    itemArr.push(
      <View horizontal={true} style={styles.comtainerItem}>
        <TouchableOpacity>
          <Image 
            source={item.image} 
            style={{
              width: 100,
              height: 80,
              resizeMode: 'contain',
              marginTop: -20
            }}
          />
          <Text 
            style={{
              textAlign: 'center',
              fontSize: 12,
              marginTop: 5
            }}
          >{item.title}</Text>
        </TouchableOpacity>
      </View>
    )
  })

  return itemArr
}

const listItem = [
  {
    id: 1,
    image: require('../images/707a23736b7ea201016c0ebf0a01a57a8706168e.png'),
    title: `Biggs' Famous BBQ Suace`
  },
  {
    id: 2,
    image: require('../images/558cdd8f9b12b640ef891cce5a95bd2bf7fd6952.png'),
    title: `Kinder's Organic mild BBQ Sauce`
  }
]

const Detail = ({ navigation }) => {
  return (
    <ScrollView style={styles.container}>
      <View style={{height: 392}}>
        <View style={styles.background}>
          <Image source={require('../images/ecee9223cdc7ddf5a1486759ff7b31d3c2b7376c.png')} style={styles.imageFood}/>
          <View style={styles.title}>
            <Text style={styles.titleFood}>HEINZ BBQ Sauce Smokey</Text>
           
              <View style={styles.category}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#f87b48',
                    fontSize: 13
                  }}
                >BBQ Sauce</Text>
              </View>
          </View>
        </View>
      </View>
      <View style={styles.containerBottom}>
        <Text style={{fontWeight: 'bold'}}>Similar products</Text>
        <View style={styles.lstItem}>
          <Items />
          <Items />
        </View>
        <View style={styles.containerBtn}>
          <TouchableOpacity style={[styles.btnBottom, styles.btnAdd]}>
            <Text style={{
              fontWeight: 'bold',
              color: '#fff'
            }}>Add ingredient</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.btnBottom, styles.btnCancel]} onPress={() => navigation.navigate('Menu')}>
            <Text style={{
              fontWeight: 'bold',
              color: '#f87b48'
            }}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  background: {
    height: 160,
    backgroundColor: '#FAF0E6',
    borderBottomLeftRadius: 110,
    borderBottomRightRadius: 110,
    flexDirection: 'column',
    alignItems: 'center'
  },
  imageFood: {
    width: 130,
    height: 210,
    resizeMode: 'contain',
    marginTop: 15,
  },
  titleFood: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 10,
  },
  category: {
    backgroundColor: '#FAF0E6',
    width: 100,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    marginTop: 10
  },
  title: {
    alignItems: 'center',
    width: 370,
    height: 90,
    borderBottomWidth: 1,
    borderColor: '#E8E8E8'
  },
  containerBottom: {
    marginLeft: 20,
    marginTop: -60
  },
  lstItem: {
    flexDirection: 'row'
  },
  comtainerItem: {
    flexDirection: 'column',
    backgroundColor: '#E8E8E8',
    width: 100,
    height: 105,
    marginRight: 10,
    marginTop: 30,
    borderRadius: 20
  },
  btnAdd: {
    backgroundColor: '#f87b48',
  },
  btnCancel: {
    backgroundColor: '#FAF0E6',
    marginBottom: 10
  },
  containerBtn: {
    alignItems: 'center',
    marginTop: 2
  },
  btnBottom: {
    marginTop: 10,
    marginLeft: -15,
    borderRadius: 10,
    width: 380,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Detail