import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import MenuBar from './menuBar'
import Icon from 'react-native-vector-icons/Entypo';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import IconAnt from 'react-native-vector-icons/AntDesign'
import IconFeather from 'react-native-vector-icons/Feather'

const Rating = (params) => {
  const rank = params.rank - 1
  let ratings = []
  for(let i = 0; i < 5; i++) {
    if (i <= rank && i >= 0)
      ratings.push(
        <IconMaterialCommunityIcons key={i + 1} name='chef-hat' style={styles.iconChefEnable}/>
      )
    else
      ratings.push(
        <IconMaterialCommunityIcons key={i + 1} name='chef-hat' style={styles.iconChefDisable}/>
      )
  }

  return ratings
}

const Level = (params) => {
  const level = params.level
  let levelArr = []
  for (let i = 0; i < 3; i++) {
    let styleColor = {}
    if (i < level && i >= 0)
      styleColor.color = '#FF7F00'
    else
      styleColor.color = '#FCD9C4'
       
    const styleIcon = `iconLevel${i + 1}`
    levelArr.push(
      <IconAnt key={i + 1} name='star' style={[styles[styleIcon], styleColor]}/>
    )
  }

  return levelArr
}
const Menu = ({ navigation }) => {
  const listItem = [
    {
      id: 1,
      image: require('../images/f49babd91101c8cccdcdeb5f5fe1952dc6a7bcd7.png'),
      level: 1,
      time: '20 m',
      rank: 4,
      point: '9,0',
      levelTxt: 'Easy'
    },
    {
      id: 2,
      image: require('../images/33a54db37ca2ffc71778ab9db39f096061f5c44d.png'),
      level: 1,
      time: '35 m',
      rank: 3,
      point: '8,0',
      levelTxt: 'Easy'
    },
    {
      id: 3,
      image: require('../images/aabf8bc6684df7d1c0a89048bfc151ed4dbf1c32.png'),
      level: 2,
      time: '1h 20 m',
      rank: 4,
      point: '9,0',
      levelTxt: 'Intermediate'
    },
    {
      id: 4,
      image: require('../images/881d853168d48c33ff7c3dde95ac632e864f299c.png'),
      level: 3,
      time: '2h',
      rank: 2,
      point: '9,0',
      levelTxt: 'Hard'
    }
  ]

  return (
    <View style={styles.container}>
      <Text style={styles.txtMenu}>MENU</Text>
      <View style={styles.lstHead}>
        <View style={styles.txtResult}>
          <Text style={{
            fontWeight: 'bold',
            fontSize: 17
          }}>
            Results: 
          </Text>
          <Text style={{
            fontWeight: 'bold',
            fontSize: 17,
            color: '#FF7F50',
            marginLeft: 5
          }}>
            14 recipes
          </Text>
        </View>
        <View style={styles.viewSort}>
          <TouchableOpacity style={styles.btnSort}>
            <Text style={{
              fontWeight: 'bold',
              fontSize: 17
            }}>
              Sort by: 
            </Text>
            <Text style={{
              fontWeight: 'bold',
              fontSize: 17,
              color: '#FF7F50',
              marginLeft: 5
            }}>
              Popular
            </Text>
            <Icon name='chevron-down'style={styles.iconDown}/>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView style={styles.listFood}>
        {
          listItem.map(item => (
            <View key={item.id} style={styles.itemFood}>
              <Image source={item.image} style={styles.imgFood}/>
              <View style={styles.infoFood}>
                <Text style={{fontWeight: 'bold', fontSize: 18}}>Recipe name</Text>
                <View style={styles.rating}>
                  <Rating rank={item.rank}/>
                  <Text style={styles.point}>{item.point}</Text>
                </View>
                <View style={styles.levelAndTime}>
                  <View style={styles.level}>
                    <Level level={item.level}/>
                  </View>
                  <View style={styles.info}>
                    <Text style={styles.levelText}>{item.levelTxt}</Text>
                    <IconFeather name='clock' style={styles.iconWatch}/>
                    <Text style={{marginLeft: 8, color: '#9C9C9C'}}>{item.time}</Text>
                  </View>
                </View>
                <View style={styles.levelAndTime}>
                  <TouchableOpacity 
                    style={[styles.btnInfo, styles.btnCheck]} 
                    onPress={() => navigation.navigate('Details')}>
                      <Text style={{
                        fontSize: 17,
                        color: '#FF7F00'
                      }}>
                        Check recipe 
                      </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[styles.btnInfo, styles.btnBookmark]}>
                    <Icon name='bookmark' style={{ fontSize: 25, color: '#FF7F00'}}/>
                  </TouchableOpacity>
                  <TouchableOpacity style={[styles.btnInfo, styles.btnBookmark]}>
                    <Icon name='share' style={{ fontSize: 25, color: '#FF7F00'}}/>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ))
        }
      </ScrollView>
    <MenuBar />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  txtMenu: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#FF7F50',
    fontSize: 20,
    marginTop: 20
  },
  lstHead: {
    marginTop: 40,
    flexDirection:'row',
    justifyContent: 'space-around'
  },
  txtResult: {
    flexDirection: 'row'
  },
  btnSort: {
    flexDirection: 'row',
    backgroundColor: '#E8E8E8',
    height: 40,
    width: 170,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -7,
    borderRadius: 15
  },
  iconDown: {
    color: '#FF7F50',
    marginLeft: 3,
    fontSize: 18,
    fontWeight: 'bold'
  },
  listFood: {
    marginTop: 15,
    flexDirection: 'column',
    height: 400,
  },
  imgFood: {
    width: 130,
    height: 150,
    borderRadius: 10
  },
  itemFood: {
    marginLeft: 25,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  infoFood: {
    marginLeft: 15
  },
  rating: {
    flexDirection: 'row',
    marginTop: 8
  },
  iconChefEnable: {
    fontSize: 20,
    color: '#FF7F00'
  },
  iconChefDisable: {
    fontSize: 20,
    color: '#FCD9C4'
  },
  point: {
    color: '#FF7F00',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 5,
    marginTop: -2
  },
  level: {
    flexDirection: 'row',
    marginTop: 10,
    width: 40,
    height: 40,
  },
  iconLevel1: {
    position: 'absolute',
    top: 17,
    fontSize: 18
  },
  iconLevel2: {
    position: 'absolute',
    left: 10,
    fontSize: 18
  },
  iconLevel3: {
    position: 'absolute',
    top: 17,
    left: 20,
    fontSize: 18
  },
  levelAndTime: {
    flexDirection: 'row',
  },
  levelText: {
    color: '#9C9C9C',
    fontSize: 15
  },
  info: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 8
  },
  iconWatch: {
    marginLeft: 10,
    fontSize: 20,
    color: '#FF7F00'
  },
  btnInfo: {
    backgroundColor: '#FDE2CA',
    marginTop: 5,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
    height: 40,
  },
  btnCheck: {
    width: 130,
  },
  btnBookmark: {
    width: 40,
  }
})

export default Menu