import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  Button
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import IconFeather from 'react-native-vector-icons/Feather'

const MenuBar = () => {
  return (
    <View style={styles.container}> 
      <View style={styles.menu}>
        <TouchableOpacity style={styles.btnMenu}>
          <Icon name='home' style={styles.icon}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnMenu}>
          <Icon name='profile'style={styles.icon}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnSearch}>
          <Icon name='search1'style={styles.iconSearch}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnMenu}>
          <IconFeather name='bookmark'style={styles.icon}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnMenu}>
          <Icon name='setting'style={styles.icon}/>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  menu: {
    backgroundColor: '#fff',
    width: 420,
    height: 60,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: -12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 24,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  btnMenu: {
    width: 40,
    height: 40,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    fontSize: 30,
    color: '#FF7F50'
  },
  btnSearch: {
    width: 65,
    height: 65,
    borderRadius: 20,
    backgroundColor: '#FF7F50',
    justifyContent: 'center',
    alignItems: 'center',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 24,
    marginTop: -63
  },
  iconSearch: {
    fontSize: 30,
    color: '#fff',
  }
})

export default MenuBar