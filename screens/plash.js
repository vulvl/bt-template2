import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  Button
} from 'react-native';

class Plash extends Component {
  componentDidMount() {
    if (this.props.route.name == 'Plash')
      setTimeout(() => {
          this.props.navigation.navigate('Menu')
      }, 2000)
  }
  
  
  render () {
    return (
      <View style={styles.container}>
        <Image
          source={require('../images/plash.png')}
          style={styles.image}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    width: 420,
  }
})

export default Plash